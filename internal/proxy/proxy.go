package proxy

import (
	"context"
	"fmt"
	"net"
	"os"
	"time"

	"github.com/armon/go-socks5"
	"gitlab.inria.fr/pmorillo/pacsocks/internal/config"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
)

type Proxy struct {
	Port            int
	Ready           chan bool
	ConnectionRetry int
	WaitAndRetry    time.Duration
	Server          *config.SSHServer
	done            chan error
	client          *ssh.Client
	reconnect       chan error
}

// EmptyResolver struct
type EmptyResolver struct{}

// Resolve resolve function for EmptyResover
func (EmptyResolver) Resolve(ctx context.Context, name string) (context.Context, net.IP, error) {
	return ctx, nil, nil
}

func New(server config.SSHServer) (proxy *Proxy, err error) {
	proxy = &Proxy{
		Ready:     make(chan bool, 1),
		Server:    &server,
		reconnect: make(chan error, 1),
		done:      make(chan error, 1),
	}
	if err := proxy.getFreePort(); err != nil {
		return nil, err
	}

	return proxy, nil
}

func (p *Proxy) Start() error {
	p.connect()

	for {
		select {
		case err := <-p.reconnect:
			if err != nil {
				fmt.Printf("Reconnecting to ssh server %v\n", p.Server.Hostname)
				p.client.Close()
				p.connect()
			}
		case err := <-p.done:
			if p.client != nil {
				fmt.Printf("Close ssh connection : %v", err)
				p.client.Close()
			}
			return err
		}
	}
}

func (p *Proxy) connect() {
	var err error

	if err = p.dial(); err != nil {
		fmt.Println(err)
		p.done <- err
		return
	}

	conf := &socks5.Config{
		Dial: func(ctx context.Context, network, addr string) (net.Conn, error) {
			return p.client.Dial(network, addr)
		},
		Resolver: EmptyResolver{},
	}

	serverSocks, err := socks5.New(conf)
	if err != nil {
		fmt.Println(err)
		p.done <- err
	}

	socks5Addr := fmt.Sprintf("localhost:%d", p.Port)

	go func() {
		fmt.Printf("Socks proxy for %s : %s\n", p.Server.Hostname, socks5Addr)
		p.Ready <- true
		if err := serverSocks.ListenAndServe("tcp", socks5Addr); err != nil {
			fmt.Println("failed to create socks5 server\n", err)
			p.done <- err
		}
	}()

}

func (p *Proxy) dial() error {
	if p.client != nil {
		p.client.Close()
	}

	c, err := sshClientConfig(*p.Server)
	if err != nil {
		return fmt.Errorf("error: %v", err)
	}

	retries := 0
	for {
		if p.ConnectionRetry > 0 && retries == p.ConnectionRetry {
			return fmt.Errorf("Error while connecting to ssh server, too many reties")
		}
		addr := fmt.Sprintf("%s:%d", p.Server.Hostname, p.Server.Port)
		p.client, err = ssh.Dial("tcp", addr, c)
		if err != nil {
			fmt.Printf("Error while connecting to ssh server : %v", err)
			if p.ConnectionRetry < 0 {
				break
			}
			retries = retries + 1
			time.Sleep(p.WaitAndRetry)
			continue
		}
		break
	}

	if p.ConnectionRetry > 0 {
		go p.WaitAndReconnect()
	}

	fmt.Printf("Connection to ssh server %s is established\n", p.Server.Hostname)

	return nil
}

func (p *Proxy) Close() {
	p.client.Close()
}

func (p *Proxy) WaitAndReconnect() {
	p.reconnect <- p.client.Wait()
}

func (p *Proxy) getFreePort() error {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		return err
	}

	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return err
	}
	defer l.Close()
	p.Port = l.Addr().(*net.TCPAddr).Port

	return nil
}

func sshAgent() ssh.AuthMethod {
	if sshAgent, err := net.Dial("unix", os.Getenv("SSH_AUTH_SOCK")); err == nil {
		return ssh.PublicKeysCallback(agent.NewClient(sshAgent).Signers)
	}
	return nil
}

func sshClientConfig(server config.SSHServer) (*ssh.ClientConfig, error) {
	return &ssh.ClientConfig{
		User:            server.Username,
		Auth:            []ssh.AuthMethod{sshAgent()},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}, nil
}
