package config

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/mitchellh/go-homedir"
	"gopkg.in/yaml.v3"
)

const (
	ConfigPath = "~/.pacsocks.yml"
)

type Config struct {
	SSHServers      []SSHServer `yaml:"sshServers"`
	Contexts        []Context   `yaml:"contexts"`
	HTTPAddr        string      `yaml:"httpAddr"`
	NetworkServices []string    `yaml:"networkServices,omitempty"`
}

type SSHServer struct {
	Name     string `yaml:"name" survey:"name"`
	Hostname string `yaml:"hostname" survey:"hostname"`
	Port     int16  `yaml:"port" survey:"port"`
	Username string `yaml:"username" survey:"username"`
}

type Context struct {
	Name           string   `yaml:"name" survey:"name"`
	SSHServerNames []string `yaml:"ssh_server_names" survey:"ssh_server_names"`
	PACFile        string   `yaml:"pacfile"`
}

func Load() (Config, error) {
	conf := Config{}
	f, _ := homedir.Expand(ConfigPath)
	if err := fileExistsOrCreate(); err != nil {
		return conf, err
	}

	yamlStr, err := ioutil.ReadFile(f)
	if err != nil {
		return conf, err
	}
	if err = yaml.Unmarshal(yamlStr, &conf); err != nil {
		return conf, err
	}
	return conf, nil
}

func (c *Config) Save() error {
	yaml, err := c.ToYAML()
	if err != nil {
		return err
	}
	f, err := homedir.Expand(ConfigPath)
	if err != nil {
		return err
	}
	if err = ioutil.WriteFile(f, yaml, 0644); err != nil {
		return err
	}
	return nil
}

func (c *Config) ToYAML() ([]byte, error) {
	yaml, err := yaml.Marshal(&c)
	if err != nil {
		return []byte{}, err
	}
	return yaml, nil
}

func (c *Config) ContextList() []string {
	contextList := make([]string, 0)
	for _, context := range c.Contexts {
		contextList = append(contextList, context.Name)
	}
	return contextList
}

func (c *Config) GetContext(ctxname string) (Context, error) {
	context := Context{}
	contextExist := false
	for _, ctx := range c.Contexts {
		if ctx.Name == ctxname {
			context = ctx
			contextExist = true
		}
	}
	if !contextExist {
		return context, fmt.Errorf("Context %s does not exist", ctxname)
	}
	return context, nil
}

func (c *Config) SSHServerList() []string {
	serverList := make([]string, 0)
	for _, server := range c.SSHServers {
		serverList = append(serverList, server.Name)
	}
	return serverList
}

func (c *Config) GetSSHServer(servername string) (SSHServer, error) {
	sshserver := SSHServer{}
	sshserverExists := false
	for _, server := range c.SSHServers {
		if server.Name == servername {
			sshserver = server
			sshserverExists = true
		}
	}
	if !sshserverExists {
		return sshserver, fmt.Errorf("SSH server %s does not exist", servername)
	}
	return sshserver, nil
}

func (c *Config) GetSSHServersForContextName(ctxname string) (sshservers []SSHServer) {
	ctx, _ := c.GetContext(ctxname)
	for _, i := range ctx.SSHServerNames {
		for _, x := range c.SSHServers {
			if i == x.Name {
				sshservers = append(sshservers, x)
			}
		}
	}

	return sshservers
}

func fileExistsOrCreate() error {
	f, _ := homedir.Expand(ConfigPath)
	if _, err := os.Stat(f); os.IsNotExist(err) {
		_, err = os.Create(f)
		if err != nil {
			return err
		}
	}
	return nil
}
