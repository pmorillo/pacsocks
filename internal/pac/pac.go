package pac

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strings"

	"gitlab.inria.fr/pmorillo/pacsocks/internal/proxy"
)

type PAC struct {
	Addr    string
	File    string
	Proxies []*proxy.Proxy
	server  *http.Server
}

func New(addr string, file string) (pac *PAC) {
	pac = &PAC{
		Addr: addr,
		File: file,
	}
	srv := &http.Server{
		Addr: pac.Addr,
	}
	pac.server = srv

	return pac
}

func (p *PAC) Start() {
	http.HandleFunc("/proxy.pac", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/x-ns-proxy-autoconfig")
		fmt.Fprint(w, p.generateFile())
	})
	http.HandleFunc("/env/", p.envHandler)

	fmt.Printf("Starting PAC HTTP server listen on http://%s\n", p.server.Addr)
	log.Fatal(p.server.ListenAndServe())
}

func (p *PAC) Stop() {
	p.server.Shutdown(context.TODO())
}

func (p *PAC) generateFile() string {
	file := p.File
	for _, p := range p.Proxies {
		file = strings.Replace(file, fmt.Sprintf("$$%s$$", p.Server.Name), fmt.Sprintf("%d", p.Port), -1)
	}

	return file
}

func (p *PAC) envHandler(w http.ResponseWriter, req *http.Request) {
	arg := req.URL.Path[len("/env/"):]
	port := 0
	for _, v := range p.Proxies {
		if arg == v.Server.Name {
			port = v.Port
		}
	}
	fmt.Fprintf(w, "export HTTP_PROXY=socks5://localhost:%d\n", port)
	fmt.Fprintf(w, "export HTTPS_PROXY=socks5://localhost:%d\n", port)
}
