package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.inria.fr/pmorillo/pacsocks/internal/config"
)

var (
	envCmd = &cobra.Command{
		Use:   "env",
		Short: "generate environnement var setting for an SSH proxy",
	}
)

func envExecute(cmd *cobra.Command, args []string) {
	conf, err := config.Load()
	if err != nil {
		fmt.Printf("Error loading config : %s", err)
		os.Exit(1)
	}
	if len(args) != 1 {
		fmt.Println("Need one argument with SSH conection identifier")
		os.Exit(1)
	}
	sshserver := config.SSHServer{}
	for _, s := range conf.SSHServers {
		if s.Name == args[0] {
			sshserver = s
		}
	}
	fmt.Printf("export HTTP_PROXY=socks5://localhost:%d", sshserver.Port)
	fmt.Printf("export HTTPS_PROXY=socks5://localhost:%d", sshserver.Port)
}
