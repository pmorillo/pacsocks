package cmd

import (
	"github.com/spf13/cobra"
)

var (
	rootCmd = &cobra.Command{
		Use:   "pacsocks",
		Short: "Multi socks proxy and PAC file HTTP server",
	}
)

func init() {
	rootCmd.AddCommand(versionCmd)
	rootCmd.AddCommand(configCmd)
	rootCmd.AddCommand(startCmd)
	configCmd.AddCommand(configAddSSHServerCmd)
	configCmd.AddCommand(configListSSHServersCmd)
	configCmd.AddCommand(configAddContextCmd)
}

func Execute() error {
	return rootCmd.Execute()
}
