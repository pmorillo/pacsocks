package cmd

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strings"

	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/cobra"
	"gitlab.inria.fr/pmorillo/pacsocks/internal/config"
)

var (
	configCmd = &cobra.Command{
		Use:   "config",
		Short: "Modification du fichier de configuration",
	}
	configAddSSHServerCmd = &cobra.Command{
		Use:   "add-sshserver",
		Short: "Add an ssh server",
		Run:   configAddSSHServerExecute,
	}
	configListSSHServersCmd = &cobra.Command{
		Use:   "list-sshservers",
		Short: "List SSH servers",
		Run:   configListSSHServersExecute,
	}
	configAddContextCmd = &cobra.Command{
		Use:   "add-context",
		Short: "Add a context",
		Run:   configAddContextExecute,
	}
)

func configAddSSHServerExecute(cmd *cobra.Command, args []string) {
	answers := config.SSHServer{}

	currentConf, err := config.Load()
	if err != nil {
		fmt.Printf("Error loading current config : %v", err)
		os.Exit(1)
	}

	qs := []*survey.Question{
		{
			Name: "name",
			Prompt: &survey.Input{
				Message: "SSH server identifier",
			},
		},
		{
			Name: "hostname",
			Prompt: &survey.Input{
				Message: "SSH server hostname",
			},
		},
		{
			Name: "port",
			Prompt: &survey.Input{
				Message: "SSH port",
				Default: "22",
			},
		},
		{
			Name: "username",
			Prompt: &survey.Input{
				Message: "SSH username",
				Default: os.Getenv("USER"),
			},
		},
	}

	if err := survey.Ask(qs, &answers); err != nil {
		fmt.Printf("Error parsing answers : %v", err)
		os.Exit(1)
	}

	currentConf.SSHServers = append(currentConf.SSHServers, answers)

	if err = currentConf.Save(); err != nil {
		fmt.Printf("Error saving configuration : %s", err)
		os.Exit(1)
	}

}

func configListSSHServersExecute(cmd *cobra.Command, args []string) {
	currentConf, err := config.Load()
	if err != nil {
		fmt.Printf("Error loading config file %s : %v", config.ConfigPath, err)
		os.Exit(1)
	}
	for _, c := range currentConf.SSHServers {
		fmt.Printf("Name : %s, Address : %s\n", c.Name, c.Hostname)
	}
}

func configAddContextExecute(cmd *cobra.Command, args []string) {
	answers := config.Context{}

	currentConf, err := config.Load()
	if err != nil {
		fmt.Printf("Error loading current config : %v", err)
		os.Exit(1)
	}
	if len(currentConf.SSHServers) < 1 {
		fmt.Printf("No SSH server defined")
		os.Exit(1)
	}

	serverNamesOptions := make([]string, 0)
	for _, s := range currentConf.SSHServers {
		serverNamesOptions = append(serverNamesOptions, s.Name)
	}

	qs := []*survey.Question{
		{
			Name: "name",
			Prompt: &survey.Input{
				Message: "Context name identifier",
				Default: "default",
			},
		},
		{
			Name: "ssh_server_names",
			Prompt: &survey.MultiSelect{
				Message: "Select SSH servers",
				Options: serverNamesOptions,
			},
		},
	}

	if err := survey.Ask(qs, &answers); err != nil {
		fmt.Printf("Error parsing answers : %s\n", err)
		os.Exit(1)
	}

	pacfile, err := configFileFromEditor([]byte(answers.PACFile))
	if err != nil {
		fmt.Printf("Failed to edit pac file : %s", err)
		os.Exit(1)
	}
	answers.PACFile = string(pacfile[:])

	currentConf.Contexts = append(currentConf.Contexts, answers)

	if err = currentConf.Save(); err != nil {
		fmt.Printf("Error saving configuration : %s\n", err)
		os.Exit(1)
	}

}

func configFileFromEditor(src []byte) ([]byte, error) {
	file, err := ioutil.TempFile(os.TempDir(), "*")
	if err != nil {
		return []byte{}, err
	}

	filename := file.Name()
	defer os.Remove(filename)

	if err = ioutil.WriteFile(filename, src, 0644); err != nil {
		return []byte{}, err
	}

	if err = file.Close(); err != nil {
		return []byte{}, err
	}

	if err = openEditor(filename); err != nil {
		return []byte{}, err
	}

	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return []byte{}, err
	}

	return bytes, nil
}

func openEditor(filename string) error {
	ed := os.Getenv("EDITOR")
	if ed == "" {
		ed = "vim"
	}
	cmdPath, err := exec.LookPath(ed)
	if err != nil {
		return err
	}
	cmd := exec.Command(cmdPath, filename)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func configChecks(conf *config.Config) {
	if conf.HTTPAddr == "" {
		prompt := &survey.Input{
			Message: "PAC file HTTP server address",
			Default: "localhost:8082",
		}
		survey.AskOne(prompt, &conf.HTTPAddr)
		if err := conf.Save(); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}
	if runtime.GOOS == "darwin" {
		if len(conf.NetworkServices) == 0 {
			prompt := &survey.MultiSelect{
				Message: "Select macOS networkservices to manage",
				Options: listAllNetworkServices(),
			}
			survey.AskOne(prompt, &conf.NetworkServices)
			if err := conf.Save(); err != nil {
				log.Fatal(err)
			}
		}
	}
}

func listAllNetworkServices() []string {
	test, err := exec.Command("/usr/sbin/networksetup", "-listallnetworkservices").Output()
	if err != nil {
		log.Fatal(err)
	}
	result := strings.Split(string(test), "\n")
	return result[1 : len(result)-1]
}
