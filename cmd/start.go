package cmd

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/cobra"
	"gitlab.inria.fr/pmorillo/pacsocks/internal/config"
	"gitlab.inria.fr/pmorillo/pacsocks/internal/pac"
	"gitlab.inria.fr/pmorillo/pacsocks/internal/proxy"
)

var (
	startCmd = &cobra.Command{
		Use:   "start",
		Short: "Start SSH connections and PAC file HTTP server",
		Run:   startExecute,
	}
)

func startExecute(cmd *cobra.Command, args []string) {
	var contextname string = ""

	fmt.Printf("Loading configuration from file %s\n", config.ConfigPath)
	config, err := config.Load()
	if err != nil {
		fmt.Printf("Error loading config : %s", err)
		os.Exit(1)
	}

	configChecks(&config)

	if len(args) == 0 {
		qs := []*survey.Question{
			{
				Name: "context",
				Prompt: &survey.Select{
					Message: "Select context",
					Options: config.ContextList(),
				},
			},
		}
		answer := struct {
			Context string
		}{}
		err := survey.Ask(qs, &answer)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		contextname = answer.Context
	} else {
		contextname = args[0]
	}

	context, err := config.GetContext(contextname)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	}

	pac := pac.New(config.HTTPAddr, context.PACFile)
	go pac.Start()

	for _, s := range config.GetSSHServersForContextName(context.Name) {
		p, err := proxy.New(s)
		if err != nil {
			fmt.Printf("Failed to create proxy for ssh server %s : %s", s.Name, err)
		}
		p.ConnectionRetry = 5
		pac.Proxies = append(pac.Proxies, p)
		p.WaitAndRetry = time.Duration(5 * time.Second)
		go p.Start()
	}

	go func() {
		time.Sleep(5 * time.Second)
		if err := proxySystemSetting(config); err != nil {
			fmt.Printf("Failed to update system wide proxy settings : %v", err)
		}
	}()

	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	<-ch

	if err = proxySystemSettingRemove(config); err != nil {
		fmt.Println(err)
	}
}

func proxySystemSetting(conf config.Config) error {
	if runtime.GOOS == "darwin" {
		for _, net := range conf.NetworkServices {
			fmt.Printf("Activate system wide proxy settings for network service %s\n", net)
			_, err := exec.Command("/usr/sbin/networksetup", "-setautoproxyurl", net, fmt.Sprintf("http://%s/proxy.pac", conf.HTTPAddr)).Output()
			if err != nil {
				log.Fatal(err)
			}
			_, err = exec.Command("/usr/sbin/networksetup", "-setautoproxystate", net, "on").Output()
			if err != nil {
				log.Fatal(err)
			}
		}
	}
	return nil
}

func proxySystemSettingRemove(conf config.Config) error {
	if runtime.GOOS == "darwin" {
		for _, net := range conf.NetworkServices {
			fmt.Printf("Disable system wide proxy settings for network service %s\n", net)
			_, err := exec.Command("/usr/sbin/networksetup", "-setautoproxystate", net, "off").Output()
			if err != nil {
				log.Fatal(err)
			}
		}
	}
	return nil
}
