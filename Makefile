# Makefile
#

APP ?= pacsocks

VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
            cat $(CURDIR)/VERSION 2> /dev/null || echo v0)
TIME ?= $(shell date +%Y%m%d%H%M%S)

.PHONY: all
all: build install

build:
	@echo === go build $(APP)
	mkdir -p ./bin
	go build \
		-ldflags "-X 'gitlab.inria.fr/pmorillo/pacsocks/cmd.buildVersion=$(VERSION)' -X 'gitlab.inria.fr/pmorillo/pacsocks/cmd.buildTime=$(TIME)'" \
		-o ./bin/$(APP) main.go

run:
	@echo === go run $(APP)
	go run \
		-ldflags "-X 'gitlab.inria.fr/pmorillo/pacsocks/cmd.buildVersion=$(VERSION)' -X 'gitlab.inria.fr/pmorillo/pacsocks/cmd.buildTime=$(TIME)'" \
		main.go version

install:
	@echo === install $(APP)
	cp ./bin/$(APP) /usr/local/bin/
	chmod a+x /usr/local/bin/$(APP)