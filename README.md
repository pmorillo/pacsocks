# PACSocks

PACSocks is a command line tools to manage multiple SOCKS proxy over SSH in a resilient way, and locally serve a [pacfile](https://en.wikipedia.org/wiki/Proxy_auto-config) over HTTP.
PACSocks is an alternalv to VPN if you need to use several VPN to access private HTTP servers.

## Download

Check the [releases](https://gitlab.inria.fr/pmorillo/pacsocks/-/releases) page.

## Usage

### Use case 

You work in a lab (lab.com) and you need to access to a private (testbed.com). At office you only need to route HTTP traffic for the tesbed, ad home, you need to route HTTP traffic for both.

### Configuration

Add SSH servers configuration to access to your lab and the tesbed. Only SSH key based authentification is supported with an SSH agent.

```sh
❯ pacsocks config add-sshserver 
? SSH server identifier lab
? SSH server hostname bastion.lab.com
? SSH port 22
? SSH username username
```

```sh
❯ pacsocks config add-sshserver
? SSH server identifier testbed
? SSH server hostname bastion.tested.com
? SSH port 22
? SSH username username
```

Add contexts : 

```sh
❯ pacsocks config add-context  
? Context name identifier home
? Select SSH servers  [Use arrows to move, space to select, <right> to all, <left> to none, type to filter]
  [x]  lab
> [x]  testbed
```

pacsocks open an editor to fill you pacfile template : 

```javascript
function FindProxyForURL(url, host) {
    if (shExpMatch(host, '*.testbed.com')) {
      return "SOCKS localhost:$$testbed$$; SOCKS5 localhost:$$testbed$$"
    } else if (shExpMatch(host, '*.lab.com')) {
      return "SOCKS localhost:$$lab$$; SOCKS5 localhost:$$lab$$"
    } else {
      return "DIRECT";
    }
}
```

> HTTP pacfile server will dynamically replace `$$sshserver_id$$` by the SOCKS port opened by PACSocks.

```sh
❯ pacsocks config add-context
? Context name identifier lab
? Select SSH servers  [Use arrows to move, space to select, <right> to all, <left> to none, type to filter]
  [ ]  lab
> [x]  testbed
```

```javascript
function FindProxyForURL(url, host) {
    if (shExpMatch(host, '*.testbed.com')) {
      return "SOCKS localhost:$$testbed$$; SOCKS5 localhost:$$testbed$$"
    } else {
      return "DIRECT";
    }
}
```

You can modify the configuration by editing the file `~/.pacsocks.yml`.

### Start

The first time you start, PACSocks will ask you the address to serve pacfile : 

```sh
❯ pacsocks start             
Loading configuration from file ~/.pacsocks.yml
? PAC file HTTP server address (localhost:8082)
```

On macOS, PACSocks will ask you on which network interfaces you want to configure HTTP proxy at system wide level : 

```sh
? Select macOS networkservices to manage  [Use arrows to move, space to select, <right> to all, <left> to none, type to filter]
  [x]  USB 10/100/1000 LAN
> [x]  Wi-Fi
  [ ]  Bluetooth PAN
  [ ]  Thunderbolt Bridge
```

Select your context : 

```sh
? Select context  [Use arrows to move, type to filter]
> home
  lab
```

```sh
Starting PAC HTTP server listen on http://localhost:8082
Connection to ssh server bastion.testbed.com is established
Socks proxy for bastion.testbed.com : localhost:55756
Activate system wide proxy settings for network service USB 10/100/1000 LAN
Activate system wide proxy settings for network service Wi-Fi
Connection to ssh server bastion.lab.com is established
Socks proxy for bastion.lab.com : localhost:55755
```

Test pacfile HTTP server : 

```sh
❯ curl http://localhost:8082/pacfile
function FindProxyForURL(url, host) {
    if (shExpMatch(host, '*.testbed.com')) {
      return "SOCKS localhost:55756; SOCKS5 localhost:55756"
    } else if (shExpMatch(host, '*.lab.com')) {
      return "SOCKS localhost:55755; SOCKS5 localhost:55755"
    } else {
      return "DIRECT";
    }
}
```

On linux you will need to configure your browser to use Automatic proxy configuration URL : http://localhost:8082.

### Set terminal environment variable

`curl http://localhost:8082/env/sshserver_id` will give you env vars exports.

```sh
❯ curl http://localhost:8082/env/testbed
export HTTP_PROXY=socks5://localhost:55756
export HTTPS_PROXY=socks5://localhost:55756
# Source this commands
source <(curl localhost:8082/env/testbed)
# Eg. use the Kubernetes cli through Socks proxy
❯ export KUBECONFIG=$(pwd)/kube_config_cluster.yml
❯ kubectl get nodes                                                                            
NAME                            STATUS   ROLES               AGE   VERSION
parasilo-6.rennes.grid5000.fr   Ready    controlplane,etcd   78s   v1.20.6
parasilo-7.rennes.grid5000.fr   Ready    worker              69s   v1.20.6
parasilo-8.rennes.grid5000.fr   Ready    worker              68s   v1.20.6
parasilo-9.rennes.grid5000.fr   Ready    worker              69s   v1.20.6
```

### Self healing

If an SSH connection is closed, PACSocks will try to reconnect automatically : 

```sh
# ...
Reconnecting to ssh server bastion.testbed.com
Connection to ssh server bastion.testbed.com is established
Socks proxy for bastion.testbed.com : localhost:55756
```