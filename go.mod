module gitlab.inria.fr/pmorillo/pacsocks

go 1.15

require (
	github.com/AlecAivazis/survey/v2 v2.2.9
	github.com/armon/go-socks5 v0.0.0-20160902184237-e75332964ef5
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.3
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
